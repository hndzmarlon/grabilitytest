package com.hndzdeveloper.marlon.grabilitymovies.interfaces;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.view.Menu;
import android.widget.RelativeLayout;

import com.hndzdeveloper.marlon.grabilitymovies.models.Movie;

import java.util.List;

/**
 * Created by Marlon on 9/21/2017.
 */

public interface HomePresenterInterface {

    void onCreate(Context context, CoordinatorLayout rootView, HomeInterface homeInterface);
    void getListMovies();
    void saveMovies(List<Movie> movieList);
}


