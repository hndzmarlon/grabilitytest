package com.hndzdeveloper.marlon.grabilitymovies.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.hndzdeveloper.marlon.grabilitymovies.views.fragments.MovieFragment;
import com.hndzdeveloper.marlon.grabilitymovies.views.fragments.TitleFragment;

import java.util.List;

/**
 * Created by Marlon on 9/20/2017.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {

    List<TitleFragment> fragmentList;

    public PagerAdapter(FragmentManager fm, List<TitleFragment> fragmentList) {
        super(fm);
        this.fragmentList = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {return fragmentList.get(position);}

    @Override
    public int getCount() {return fragmentList.size();}

    @Override
    public CharSequence getPageTitle(int position){ return fragmentList.get(position).getTitle(); }
}
