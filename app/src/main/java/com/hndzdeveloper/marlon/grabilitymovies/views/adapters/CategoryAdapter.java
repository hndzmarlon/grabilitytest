package com.hndzdeveloper.marlon.grabilitymovies.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hndzdeveloper.marlon.grabilitymovies.R;
import com.hndzdeveloper.marlon.grabilitymovies.helpers.Constants;
import com.hndzdeveloper.marlon.grabilitymovies.models.Movie;
import com.hndzdeveloper.marlon.grabilitymovies.views.DetailActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marlon on 9/20/2017.
 */

public class CategoryAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> implements MovieAdapter.IActionMovie {

    static final int POPULAR = 0;
    static final int TOP_RATED = 1;
    static final int UPCOMING = 2;

    Context context;
    String [] categoryList;
    List<Movie> movieList;

    MovieAdapter movieAdapter;

    public CategoryAdapter(Context context, String[] categoryList, List<Movie> movieList) {
        this.context = context;
        this.categoryList = categoryList;
        this.movieList = movieList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;

        switch (viewType){
            case POPULAR:
                View v0 = View.inflate(context, R.layout.template_category,null);
                viewHolder = new PopularViewHolder(v0);
                break;
            case TOP_RATED:
                View v1 = View.inflate(context, R.layout.template_category,null);
                viewHolder = new TopRatedViewHolder(v1);
                break;
            case UPCOMING:
                View v2 = View.inflate(context, R.layout.template_category,null);
                viewHolder = new UpcomingViewHolder(v2);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        String title = categoryList[position];

        if (holder instanceof PopularViewHolder){
            PopularViewHolder popularViewHolder = (PopularViewHolder) holder;
            popularViewHolder.categoryTitle.setText(title);

            List<Movie> popularMovies = new ArrayList<>();
            for (Movie m: movieList){
                if (m.getCategory().equals(Constants.POPULAR_CATEGORY)){
                    popularMovies.add(m);
                }
            }

            if (popularMovies.size()<=0){
                popularViewHolder.cardPopular.setVisibility(View.GONE);
            }

            movieAdapter = new MovieAdapter(context,popularMovies,this);
            popularViewHolder.recMovieList.setAdapter(movieAdapter);
            popularViewHolder.recMovieList
                    .setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false));

        }
        //***//
        if (holder instanceof TopRatedViewHolder){
            TopRatedViewHolder topRatedViewHolder = (TopRatedViewHolder) holder;
            topRatedViewHolder.categoryTitle.setText(title);

            List<Movie> topRatedMovies = new ArrayList<>();
            for (Movie m: movieList){
                if (m.getCategory().equals(Constants.TOP_RATED_CATEGORY)){
                    topRatedMovies.add(m);
                }
            }
            if (topRatedMovies.size()<=0){
                topRatedViewHolder.cardTopRated.setVisibility(View.GONE);
            }
            movieAdapter = new MovieAdapter(context,topRatedMovies,this);
            topRatedViewHolder.recMovieList.setAdapter(movieAdapter);
            topRatedViewHolder.recMovieList
                    .setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false));

        }
        //***//
        if (holder instanceof UpcomingViewHolder){
            UpcomingViewHolder upcomingViewHolder = (UpcomingViewHolder) holder;
            upcomingViewHolder.categoryTitle.setText(title);

            List<Movie> upcomingMovies = new ArrayList<>();
            for (Movie m: movieList){
                if (m.getCategory().equals(Constants.UPCOMING_CATEGORY)){
                    upcomingMovies.add(m);
                }
            }
            if (upcomingMovies.size()<=0){
                upcomingViewHolder.cardUpcoming.setVisibility(View.GONE);
            }
            movieAdapter = new MovieAdapter(context,upcomingMovies,this);
            upcomingViewHolder.recMovieList.setAdapter(movieAdapter);
            upcomingViewHolder.recMovieList
                    .setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false));
        }
    }

    @Override
    public int getItemCount() {return categoryList.length;}

    @Override
    public int getItemViewType(int position) {
        if (position == 0){
            return  POPULAR;
        }else if (position == 1){
            return TOP_RATED;
        }else {
            return UPCOMING;
        }
    }

    /***
     * Interface from MovieAdapter
     ***/
    @Override
    public void gotoDetail(int movieId) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra("movieId",movieId);
        context.startActivity(intent);
    }

    /***
     * ViewHolders
     ***/

    //***Esto se hace en caso de querer personalizar cada categoria***//

    public class PopularViewHolder extends RecyclerView.ViewHolder{
        CardView cardPopular;
        TextView categoryTitle;
        RecyclerView recMovieList;
        public PopularViewHolder(View itemView) {
            super(itemView);
            cardPopular = (CardView) itemView.findViewById(R.id.template_card_list_category);
            categoryTitle = (TextView) itemView.findViewById(R.id.txt_title_category);
            recMovieList = (RecyclerView) itemView.findViewById(R.id.recycler_list_movies);
        }
    }
    public class TopRatedViewHolder extends RecyclerView.ViewHolder{
        CardView cardTopRated;
        TextView categoryTitle;
        RecyclerView recMovieList;
        public TopRatedViewHolder(View itemView) {
            super(itemView);
            cardTopRated = (CardView) itemView.findViewById(R.id.template_card_list_category);
            categoryTitle = (TextView) itemView.findViewById(R.id.txt_title_category);
            recMovieList = (RecyclerView) itemView.findViewById(R.id.recycler_list_movies);
        }
    }
    public class UpcomingViewHolder extends RecyclerView.ViewHolder{
        CardView cardUpcoming;
        TextView categoryTitle;
        RecyclerView recMovieList;
        public UpcomingViewHolder(View itemView) {
            super(itemView);
            cardUpcoming = (CardView) itemView.findViewById(R.id.template_card_list_category);
            categoryTitle = (TextView) itemView.findViewById(R.id.txt_title_category);
            recMovieList = (RecyclerView) itemView.findViewById(R.id.recycler_list_movies);
        }
    }

}
