package com.hndzdeveloper.marlon.grabilitymovies.presenters;

import android.content.Context;

import com.hndzdeveloper.marlon.grabilitymovies.interfaces.DetailInterface;
import com.hndzdeveloper.marlon.grabilitymovies.interfaces.DetailPresenterInterface;
import com.hndzdeveloper.marlon.grabilitymovies.models.Movie;

import io.realm.Realm;

/**
 * Created by Marlon on 9/22/2017.
 */

public class DetailPresenter implements DetailPresenterInterface{

    Context context;
    DetailInterface detailInterface;

    @Override
    public void onCreate(Context context, DetailInterface detailInterface) {
        this.context = context;
        this.detailInterface = detailInterface;
    }

    @Override
    public void getMovieData(int movieId) {
        Realm realm = Realm.getDefaultInstance();
        Movie movie = realm.where(Movie.class)
                .equalTo("id",movieId)
                .findFirst();
        detailInterface.putData(movie);


    }
}
