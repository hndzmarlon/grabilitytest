package com.hndzdeveloper.marlon.grabilitymovies.presenters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.hndzdeveloper.marlon.grabilitymovies.R;
import com.hndzdeveloper.marlon.grabilitymovies.helpers.Constants;
import com.hndzdeveloper.marlon.grabilitymovies.interfaces.MovieFragmentInterface;
import com.hndzdeveloper.marlon.grabilitymovies.interfaces.MovieFragmentPresenterInterface;
import com.hndzdeveloper.marlon.grabilitymovies.models.Movie;
import com.hndzdeveloper.marlon.grabilitymovies.views.fragments.MovieFragment;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Marlon on 9/21/2017.
 */

public class MovieFragmentPresenter implements MovieFragmentPresenterInterface{

    MovieFragmentInterface movieFragmentInterface;

    Realm realm;

    @Override
    public void onCreate(  MovieFragmentInterface movieFragmentInterface) {
        this.movieFragmentInterface = movieFragmentInterface;
    }

    @Override
    public void getLocalListMovies(int type) {
        String typeMv = "";
        if (type== MovieFragment.MOVIE_TYPE){
            typeMv = Constants.TYPE_MOVIE;
        }else if (type == MovieFragment.SERIE_TYPE){
            typeMv = Constants.TYPE_SERIE;
        }

        realm = Realm.getDefaultInstance();
        RealmResults<Movie> movies = realm.where(Movie.class)
                .equalTo("type",typeMv)
                .findAll();
        movieFragmentInterface.createList(movieFragmentInterface.createMovieAdapter(movies));
    }




}
