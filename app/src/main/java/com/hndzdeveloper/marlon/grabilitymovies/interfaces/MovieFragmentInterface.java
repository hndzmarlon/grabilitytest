package com.hndzdeveloper.marlon.grabilitymovies.interfaces;

import com.hndzdeveloper.marlon.grabilitymovies.models.Movie;
import com.hndzdeveloper.marlon.grabilitymovies.views.adapters.CategoryAdapter;

import java.util.List;

/**
 * Created by Marlon on 9/21/2017.
 */

public interface MovieFragmentInterface {
    void createList(CategoryAdapter adapter);
    CategoryAdapter createMovieAdapter(List<Movie> movieList);
}
