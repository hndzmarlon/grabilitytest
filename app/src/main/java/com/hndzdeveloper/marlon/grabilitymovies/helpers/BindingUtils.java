package com.hndzdeveloper.marlon.grabilitymovies.helpers;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.hndzdeveloper.marlon.grabilitymovies.R;
import com.squareup.picasso.Picasso;

/**
 * Created by Marlon on 9/24/2017.
 */

public class BindingUtils {

    @BindingAdapter({"bind:setImage"})
    public static void loadImage(ImageView imageView, String path){
        Picasso
                .with(imageView.getContext())
                .load(Constants.URL_IMAGES+path)
                .error(R.drawable.error_img)
                .into(imageView);

    }
}
