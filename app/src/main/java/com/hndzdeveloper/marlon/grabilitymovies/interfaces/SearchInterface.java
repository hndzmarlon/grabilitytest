package com.hndzdeveloper.marlon.grabilitymovies.interfaces;

import com.hndzdeveloper.marlon.grabilitymovies.models.Movie;
import com.hndzdeveloper.marlon.grabilitymovies.views.adapters.CategoryAdapter;

import java.util.List;

/**
 * Created by Marlon on 9/22/2017.
 */

public interface SearchInterface {
    void showResults(CategoryAdapter adapter, boolean empty);
    CategoryAdapter createCategoryAdapter(List<Movie> movieResults);
}
