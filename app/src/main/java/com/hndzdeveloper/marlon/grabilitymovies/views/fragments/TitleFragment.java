package com.hndzdeveloper.marlon.grabilitymovies.views.fragments;

import android.support.v4.app.Fragment;

/**
 * Created by Marlon on 9/22/2017.
 */

public abstract class TitleFragment extends Fragment {
    public abstract String getTitle();
}

