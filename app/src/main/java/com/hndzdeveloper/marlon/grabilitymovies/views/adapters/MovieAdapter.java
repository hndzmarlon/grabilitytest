package com.hndzdeveloper.marlon.grabilitymovies.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hndzdeveloper.marlon.grabilitymovies.R;
import com.hndzdeveloper.marlon.grabilitymovies.databinding.TemplateMovieBinding;
import com.hndzdeveloper.marlon.grabilitymovies.helpers.Constants;
import com.hndzdeveloper.marlon.grabilitymovies.models.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Marlon on 9/20/2017.
 */

public class MovieAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    Context context;
    List<Movie> movieList;
    IActionMovie iActionMovie;



    public MovieAdapter(Context context, List<Movie> movieList, IActionMovie iActionMovie) {
        this.context = context;
        this.movieList = movieList;
        this.iActionMovie = iActionMovie;
    }

    public interface IActionMovie{
        void gotoDetail(int movieId);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        TemplateMovieBinding movieBinding = TemplateMovieBinding.inflate(layoutInflater, parent,false);
        return new MovieViewHolder(movieBinding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final Movie movie = movieList.get(position);

        MovieViewHolder movieViewHolder = (MovieViewHolder) holder;
        movieViewHolder.bind(movie);

        movieViewHolder.binding.linearMovieTemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iActionMovie.gotoDetail(movie.getId());
            }
        });

    }

    @Override
    public int getItemCount() {return movieList.size();}

    /****
     * Holders
     ****/
    public class MovieViewHolder extends RecyclerView.ViewHolder{

        final TemplateMovieBinding binding;

        public MovieViewHolder(TemplateMovieBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind (Movie movie){
            binding.setMovie(movie);
            binding.executePendingBindings();
        }
    }


}
