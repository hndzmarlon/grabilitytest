package com.hndzdeveloper.marlon.grabilitymovies.interfaces;

/**
 * Created by Marlon on 9/21/2017.
 */

public interface ResultsManagerInterface {
    void actionSnack();
    void actionAlert();
}
