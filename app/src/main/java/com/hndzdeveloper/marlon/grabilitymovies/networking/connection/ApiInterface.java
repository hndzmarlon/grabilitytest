package com.hndzdeveloper.marlon.grabilitymovies.networking.connection;

import com.google.gson.JsonObject;
import com.hndzdeveloper.marlon.grabilitymovies.networking.response.MovieResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;

/**
 * Created by Marlon on 9/20/2017.
 */

public interface ApiInterface {

    @Headers({"Content-Type: application/json"})
    @GET("{type}/{category}?api_key=e8b9a40a93f81646ed55b1df103c3b13&language=en-US&page=1")
    Call<MovieResponse> getMovies(@Path("type") String type,@Path("category") String category);

}
