package com.hndzdeveloper.marlon.grabilitymovies.views;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.hndzdeveloper.marlon.grabilitymovies.R;
import com.hndzdeveloper.marlon.grabilitymovies.interfaces.SearchInterface;
import com.hndzdeveloper.marlon.grabilitymovies.interfaces.SearchPresenterInterface;
import com.hndzdeveloper.marlon.grabilitymovies.models.Movie;
import com.hndzdeveloper.marlon.grabilitymovies.presenters.SearchPresenter;
import com.hndzdeveloper.marlon.grabilitymovies.views.adapters.CategoryAdapter;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public class SearchActivity extends AppCompatActivity implements SearchInterface, SearchView.OnQueryTextListener {

    //*Viewa
    Toolbar toolbar;
    Menu mMenu;
    RecyclerView recyclerView;
    RecyclerView recCategoryList;

    LinearLayout linearHint;

    SearchPresenterInterface searchPresenterInterface;

    Window window;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        getViews();

        searchPresenterInterface = new SearchPresenter();
        searchPresenterInterface.onCreate(this,this);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void getViews() {
        //*Window
        window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(this.getResources().getColor(R.color.colorGrabilityDarker));

        //*Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar_search);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        linearHint = (LinearLayout) findViewById(R.id.linear_hint_search);
        recCategoryList = (RecyclerView) findViewById(R.id.rec_search_list);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //mMenu = menu;
        getMenuInflater().inflate(R.menu.menu_searchable,menu);

        SearchManager searchManager = (SearchManager) getSystemService(this.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_searchable).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()== android.R.id.home){finish();}

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        searchPresenterInterface.searchMovie(newText);
        return true;
    }
    @Override
    public CategoryAdapter createCategoryAdapter(List<Movie> movieResults) {
        return new CategoryAdapter(this,getResources().getStringArray(R.array.Categories),movieResults);
    }
    @Override
    public void showResults(CategoryAdapter adapter, boolean empty) {
        if (empty){
            linearHint.setVisibility(View.VISIBLE);
        }else {
            linearHint.setVisibility(View.GONE);
        }
        recCategoryList.setAdapter(adapter);
        recCategoryList.setLayoutManager(new LinearLayoutManager(this));
    }


}
