package com.hndzdeveloper.marlon.grabilitymovies.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;

import com.hndzdeveloper.marlon.grabilitymovies.R;

/**
 * Created by Marlon on 9/20/2017.
 */

public class CheckNetworkHelper {


    public static boolean connected(Context context){

        boolean net = false;

        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();

        if (activeNetwork != null) { // connected to internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                net = true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                showPositiveAlert(context);
                net = true;
            }
        } else {
            // not connected to internet
            //showNegativeAlert();
            net= false;
        }

        return net;
    }


    private static void showPositiveAlert(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.title_connection_alert)
                .setMessage(R.string.msg_mobile_data_connection)
                //.setIcon(R.drawable.ic_check_circle)
                .setNeutralButton(R.string.btn_acept,null)
                .create().show();
    }

}
