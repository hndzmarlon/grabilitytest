package com.hndzdeveloper.marlon.grabilitymovies.interfaces;

import com.hndzdeveloper.marlon.grabilitymovies.views.fragments.TitleFragment;

import java.util.List;

/**
 * Created by Marlon on 9/21/2017.
 */

public interface HomeInterface {
    List<TitleFragment> createFragmentList();
    void setFragments(List<TitleFragment> fragmentList);


}
