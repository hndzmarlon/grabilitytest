package com.hndzdeveloper.marlon.grabilitymovies.networking.connection;

import com.hndzdeveloper.marlon.grabilitymovies.helpers.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Marlon on 9/20/2017.
 */

public class ApiClient {
    public static final String BASE_URL = Constants.BASE_URL_RFT;
    private static Retrofit retrofit = null;

    public static Retrofit getClient(){
        if (retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
