package com.hndzdeveloper.marlon.grabilitymovies.helpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.hndzdeveloper.marlon.grabilitymovies.R;
import com.hndzdeveloper.marlon.grabilitymovies.interfaces.ResultsManagerInterface;

/**
 * Created by Marlon on 9/21/2017.
 */

public class ResultsManager {

    //*Views
    Context context;
    CoordinatorLayout relativeLayout;

    //*ProgressDialog
    ProgressDialog progressDialog;
    Drawable drawable;

    //*SanckBar
    Snackbar snackbar;


    static ResultsManager resultsManager;
    ResultsManagerInterface resultsManagerInterface;


    public ResultsManager() {}

    public static ResultsManager getInstance(){
        if (resultsManager==null){
            resultsManager = new ResultsManager();
        }
        return resultsManager;
    }

    public void init (Context context, CoordinatorLayout relativeLayout, ResultsManagerInterface resultsManagerInterface){
        this.context = context;
        this.relativeLayout = relativeLayout;
        this.resultsManagerInterface = resultsManagerInterface;

        drawable = new ProgressBar(context).getIndeterminateDrawable().mutate();
        drawable.setColorFilter(ContextCompat.getColor(context, R.color.colorGrabilityGreen), PorterDuff.Mode.SRC_IN);

    }

    public void initLoad(){
        try {
            dimissSnackBar();
            progressDialog = ProgressDialog.show(context,
                    context.getResources().getString(R.string.msg_please_wait),
                    context.getResources().getString(R.string.msg_loading),true);
            progressDialog.setIndeterminateDrawable(drawable);
            progressDialog.setCancelable(false);
        }catch (Exception e){
            Log.e("ResultsManger","InitLoad. Error añadir view: "+e.toString());
        }
    }

    public void loadOk(){
        //Log.e("ProgressDialig","HIDE");
        progressDialog.dismiss();
        dimissSnackBar();
    }

    public void noItems(){
        try{
            if (progressDialog!=null){
                progressDialog.dismiss();
            }
            dimissSnackBar();
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.title_connection_alert)
                    .setMessage(R.string.msg_empty_data)
                    //.setIcon(R.drawable.ic_alert)
                    .setNeutralButton(context.getResources().getString(R.string.btn_retry), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            resultsManagerInterface.actionAlert();
                        }
                    })
                    .create().show();

        }catch (Exception e){
            Log.e("ResultsManger","No Items. Error: "+e.toString());
        }
    }

    public void errorLoad(String snackMsg){
        progressDialog.dismiss();
        Snackbar.make(relativeLayout, snackMsg, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.btn_retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        resultsManagerInterface.actionSnack();
                    }
                })
                .show();

    }


    public void dimissSnackBar(){
        if (snackbar != null){
            snackbar.dismiss();
        }
    }


}
