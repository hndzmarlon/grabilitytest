package com.hndzdeveloper.marlon.grabilitymovies.interfaces;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.hndzdeveloper.marlon.grabilitymovies.models.Movie;

import java.util.List;

/**
 * Created by Marlon on 9/21/2017.
 */

public interface MovieFragmentPresenterInterface {
    void onCreate(MovieFragmentInterface movieFragmentInterface);
    void getLocalListMovies(int pos);
}
