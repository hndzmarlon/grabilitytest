package com.hndzdeveloper.marlon.grabilitymovies.interfaces;

import android.content.Context;

/**
 * Created by Marlon on 9/22/2017.
 */

public interface DetailPresenterInterface {
    void onCreate(Context context, DetailInterface detailInterface);
    void getMovieData(int movieId);
    //void loadImageDetail()
}
