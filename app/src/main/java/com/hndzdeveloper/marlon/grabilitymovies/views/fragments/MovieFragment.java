package com.hndzdeveloper.marlon.grabilitymovies.views.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hndzdeveloper.marlon.grabilitymovies.R;
import com.hndzdeveloper.marlon.grabilitymovies.interfaces.MovieFragmentInterface;
import com.hndzdeveloper.marlon.grabilitymovies.interfaces.MovieFragmentPresenterInterface;
import com.hndzdeveloper.marlon.grabilitymovies.models.Movie;
import com.hndzdeveloper.marlon.grabilitymovies.presenters.MovieFragmentPresenter;
import com.hndzdeveloper.marlon.grabilitymovies.views.HomeActivity;
import com.hndzdeveloper.marlon.grabilitymovies.views.adapters.CategoryAdapter;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MovieFragment extends TitleFragment implements MovieFragmentInterface{

    public static final int MOVIE_TYPE = 0;
    public static final int SERIE_TYPE = 1;


    int type;

    String [] categories;

    RecyclerView recCategoryList;
    MovieFragmentPresenterInterface movieFragmentPresenterInterface;

    public MovieFragment() {}

    public void init(int type){
        this.type = type;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_movie, container, false);

        categories = getResources().getStringArray(R.array.Categories);
        recCategoryList = (RecyclerView) v.findViewById(R.id.recycler_list_category);

        movieFragmentPresenterInterface = new MovieFragmentPresenter();
        movieFragmentPresenterInterface.onCreate(this);

        getLocalListMovies();


        return v;
    }

    private void getLocalListMovies() {
        movieFragmentPresenterInterface.getLocalListMovies(type);
    }

    @Override
    public CategoryAdapter createMovieAdapter(List<Movie> movieList) {
        return new CategoryAdapter(getContext(),categories,movieList);
    }

    @Override
    public void createList(CategoryAdapter adapter) {

        recCategoryList.setAdapter(adapter);
        recCategoryList.setLayoutManager(new LinearLayoutManager(getContext()));
    }


    @Override
    public String getTitle() {
        switch (type){
            case MOVIE_TYPE:
                return "Movies";
            case SERIE_TYPE:
                return "Tv Series";
            default:
                return "Movie";
        }

    }
}
