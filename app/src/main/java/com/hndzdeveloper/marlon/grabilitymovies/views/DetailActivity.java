package com.hndzdeveloper.marlon.grabilitymovies.views;

import android.databinding.DataBindingUtil;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.hndzdeveloper.marlon.grabilitymovies.R;
import com.hndzdeveloper.marlon.grabilitymovies.databinding.ActivityDetailBinding;
import com.hndzdeveloper.marlon.grabilitymovies.helpers.Constants;
import com.hndzdeveloper.marlon.grabilitymovies.interfaces.DetailInterface;
import com.hndzdeveloper.marlon.grabilitymovies.interfaces.DetailPresenterInterface;
import com.hndzdeveloper.marlon.grabilitymovies.models.Movie;
import com.hndzdeveloper.marlon.grabilitymovies.presenters.DetailPresenter;
import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity implements DetailInterface{

    DetailPresenterInterface detailPresenterInterface;
    ActivityDetailBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_detail);

        binding = DataBindingUtil.setContentView(this,R.layout.activity_detail);
        getViews();


        detailPresenterInterface = new DetailPresenter();
        detailPresenterInterface.onCreate(this,this);
        detailPresenterInterface.getMovieData(getIntent().getIntExtra("movieId",0));
    }

    private void getViews() {

        //*Toolbar
        setSupportActionBar(binding.toolbarDetail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void putData(Movie movie) {
        binding.setMovie(movie);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()== android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
