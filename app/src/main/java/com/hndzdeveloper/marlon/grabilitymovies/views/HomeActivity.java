package com.hndzdeveloper.marlon.grabilitymovies.views;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.hndzdeveloper.marlon.grabilitymovies.R;
import com.hndzdeveloper.marlon.grabilitymovies.interfaces.HomeInterface;
import com.hndzdeveloper.marlon.grabilitymovies.interfaces.HomePresenterInterface;
import com.hndzdeveloper.marlon.grabilitymovies.presenters.HomePrensenter;
import com.hndzdeveloper.marlon.grabilitymovies.views.adapters.PagerAdapter;
import com.hndzdeveloper.marlon.grabilitymovies.views.fragments.MovieFragment;
import com.hndzdeveloper.marlon.grabilitymovies.views.fragments.TitleFragment;

import java.util.ArrayList;
import java.util.List;


public class HomeActivity extends AppCompatActivity implements HomeInterface{


    //region ****  ****
    //*Views
    Toolbar toolbar;
    ViewPager viewPager;
    TabLayout tabLayout;
    NestedScrollView nestedScrollView;
    CoordinatorLayout coordinatorLayout;

    Window window;

    //*Pager
    List<TitleFragment> fragmentList;
    PagerAdapter pagerAdapter;

    //*Presenter
    HomePresenterInterface homePresenterInterface;
    //endregion


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        getViews();
        homePresenterInterface = new HomePrensenter();
        homePresenterInterface.onCreate(this, coordinatorLayout,this);
        getListMovies();

    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void getViews() {
        //*Window
        window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(this.getResources().getColor(R.color.colorGrabilityDarker));

        //*Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar_home);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);

        //*Pager
        viewPager = (ViewPager) findViewById(R.id.viewPager_home);
        tabLayout= (TabLayout) findViewById(R.id.tab_home);
        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScroll_home);
        nestedScrollView.setFillViewport(true);

        //*Coord
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.relative_home);

    }

    private void getListMovies() {
        homePresenterInterface.getListMovies();
    }

    @Override
    public List<TitleFragment> createFragmentList() {
        fragmentList = new ArrayList<>();
        MovieFragment movieFragment = new MovieFragment();
        movieFragment.init(MovieFragment.MOVIE_TYPE);

        MovieFragment seriesFragment = new MovieFragment();
        seriesFragment.init(MovieFragment.SERIE_TYPE);

        fragmentList.add(movieFragment);
        fragmentList.add(seriesFragment);

        return fragmentList;
    }

    @Override
    public void setFragments(List<TitleFragment> fragmentList) {
        pagerAdapter = new PagerAdapter(getSupportFragmentManager(),fragmentList);
        viewPager.setAdapter(pagerAdapter);
        try{
            tabLayout.setupWithViewPager(viewPager);
        }catch (RuntimeException e){
            Log.e("ErrorFragment",e.getMessage());

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_search:
                goToSearchActivity();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void goToSearchActivity(){
        Intent intent = new Intent(this,SearchActivity.class);
        startActivity(intent);
    }

}
