package com.hndzdeveloper.marlon.grabilitymovies.helpers;

import android.support.design.widget.Snackbar;
import android.view.View;

import com.hndzdeveloper.marlon.grabilitymovies.R;

import retrofit2.http.PUT;

/**
 * Created by Marlon on 9/20/2017.
 */

public class Constants {
    //ApiKey
    public static final String API_KEY_DB = "e8b9a40a93f81646ed55b1df103c3b13";

    //URLs
    public static final String BASE_URL_RFT = "https://api.themoviedb.org/3/";
    public static final String URL_IMAGES = "https://image.tmdb.org/t/p/w640";

    //Categories
    public static final String POPULAR_CATEGORY = "popular";
    public static final String TOP_RATED_CATEGORY = "top_rated";
    public static final String UPCOMING_CATEGORY = "upcoming";

    //TypeMovie
    public static final String TYPE_MOVIE = "movie";
    public static final String TYPE_SERIE = "tv";

    //TypeInformation
    public static final int MOVIE_POPULAR   = 0;
    public static final int MOVIE_TOP_RATED = 1;
    public static final int MOVIE_UPCOMING  = 2;
    public static final int SERIE_POPULAR   = 3;
    public static final int SERIE_TOP_RATED = 4;
    public static final int FULL_INFORMATION= 5;

}
