package com.hndzdeveloper.marlon.grabilitymovies.interfaces;

import android.content.Context;
import android.view.Menu;

/**
 * Created by Marlon on 9/22/2017.
 */

public interface SearchPresenterInterface {
    void onCreate(Context context, SearchInterface searchInterface);
    boolean searchMovie(String query);
}
