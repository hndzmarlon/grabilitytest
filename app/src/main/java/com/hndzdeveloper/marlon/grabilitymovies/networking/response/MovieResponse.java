package com.hndzdeveloper.marlon.grabilitymovies.networking.response;

import com.hndzdeveloper.marlon.grabilitymovies.models.Movie;

import java.util.List;

/**
 * Created by Marlon on 9/21/2017.
 */

public class MovieResponse {

    int page;
    List<Movie> results;
    int total_results;
    int total_pages;


    public MovieResponse() {
    }

    public int getPage() {
        return page;
    }
    public void setPage(int page) {
        this.page = page;
    }

    public List<Movie> getResults() {
        return results;
    }
    public void setResults(List<Movie> results) {
        this.results = results;
    }

    public int getTotal_results() {
        return total_results;
    }
    public void setTotal_results(int total_results) {
        this.total_results = total_results;
    }

    public int getTotal_pages() {
        return total_pages;
    }
    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }


}
