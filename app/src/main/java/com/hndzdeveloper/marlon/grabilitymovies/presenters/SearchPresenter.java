package com.hndzdeveloper.marlon.grabilitymovies.presenters;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.hndzdeveloper.marlon.grabilitymovies.R;
import com.hndzdeveloper.marlon.grabilitymovies.interfaces.SearchInterface;
import com.hndzdeveloper.marlon.grabilitymovies.interfaces.SearchPresenterInterface;
import com.hndzdeveloper.marlon.grabilitymovies.models.Movie;
import com.hndzdeveloper.marlon.grabilitymovies.views.adapters.CategoryAdapter;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Marlon on 9/22/2017.
 */

public class SearchPresenter implements SearchPresenterInterface{

    Context context;
    SearchInterface searchInterface;


    @Override
    public void onCreate(Context context, SearchInterface searchInterface) {
        this.context = context;
        this.searchInterface = searchInterface;
    }

    @Override
    public boolean searchMovie(String query) {
        query = query.toLowerCase();
        List<Movie> filterMovieList = new ArrayList<>();
        if (!query.isEmpty())
        {
           Realm realm = Realm.getDefaultInstance();
           List<Movie> localMovies = realm.where(Movie.class).findAll();

           for (Movie m: localMovies){
               String title = m.getOriginal_title();
               if (title!=null){
                   title.toLowerCase();
                   if (title.contains(query)){
                       filterMovieList.add(m);
                   }
               }else {
                   title = m.getName().toLowerCase();
                   if (title.contains(query)) {
                       filterMovieList.add(m);
                   }
               }
           }
        }
       searchInterface.showResults(searchInterface.createCategoryAdapter(filterMovieList),query.isEmpty());

        return false;
    }


}
