package com.hndzdeveloper.marlon.grabilitymovies.interfaces;

import com.hndzdeveloper.marlon.grabilitymovies.models.Movie;

/**
 * Created by Marlon on 9/22/2017.
 */

public interface DetailInterface {
    void putData(Movie movie);
}
