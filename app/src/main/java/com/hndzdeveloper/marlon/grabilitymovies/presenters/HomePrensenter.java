package com.hndzdeveloper.marlon.grabilitymovies.presenters;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import com.hndzdeveloper.marlon.grabilitymovies.R;
import com.hndzdeveloper.marlon.grabilitymovies.helpers.Constants;
import com.hndzdeveloper.marlon.grabilitymovies.helpers.ResultsManager;
import com.hndzdeveloper.marlon.grabilitymovies.interfaces.HomeInterface;
import com.hndzdeveloper.marlon.grabilitymovies.interfaces.HomePresenterInterface;
import com.hndzdeveloper.marlon.grabilitymovies.interfaces.ResultsManagerInterface;
import com.hndzdeveloper.marlon.grabilitymovies.models.Movie;
import com.hndzdeveloper.marlon.grabilitymovies.helpers.CheckNetworkHelper;
import com.hndzdeveloper.marlon.grabilitymovies.networking.connection.ApiClient;
import com.hndzdeveloper.marlon.grabilitymovies.networking.connection.ApiInterface;
import com.hndzdeveloper.marlon.grabilitymovies.networking.response.MovieResponse;
import com.hndzdeveloper.marlon.grabilitymovies.views.SearchActivity;
import com.hndzdeveloper.marlon.grabilitymovies.views.fragments.MovieFragment;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Marlon on 9/21/2017.
 */

public class HomePrensenter implements HomePresenterInterface, ResultsManagerInterface {

    Context context;
    ResultsManager resultsManager = ResultsManager.getInstance();

    HomeInterface homeInterface;

    List<Movie> movieList = new ArrayList<Movie>();



    @Override
    public void onCreate(Context context, CoordinatorLayout rootView, HomeInterface homeInterface) {
        this.context = context;
        this.resultsManager.init(context,rootView,this);
        this.homeInterface = homeInterface;
    }

    @Override
    public void getListMovies() {
        if (CheckNetworkHelper.connected(context)){
            resultsManager.initLoad();
            getMovies(Constants.MOVIE_POPULAR );
        }else {
            //Current Local Movies
            Realm realm = Realm.getDefaultInstance();
            RealmResults<Movie> localMovies = realm.where(Movie.class).findAll();
            boolean localMoviesEmpty = localMovies.isEmpty();
            if (localMoviesEmpty){
                resultsManager.noItems();
            }else {
                loadFragments();
            }
        }

    }

    public void getMovies(int typeInformation){

        switch (typeInformation){
            case Constants.MOVIE_POPULAR:
                makeRequest(Constants.POPULAR_CATEGORY, Constants.TYPE_MOVIE, Constants.MOVIE_TOP_RATED);
                break;
            case Constants.MOVIE_TOP_RATED:
                makeRequest(Constants.TOP_RATED_CATEGORY, Constants.TYPE_MOVIE, Constants.MOVIE_UPCOMING);
                break;
            case Constants.MOVIE_UPCOMING:
                makeRequest(Constants.UPCOMING_CATEGORY, Constants.TYPE_MOVIE, Constants.SERIE_POPULAR);
                break;
            case Constants.SERIE_POPULAR:
                makeRequest(Constants.POPULAR_CATEGORY, Constants.TYPE_SERIE, Constants.SERIE_TOP_RATED);
                break;
            case Constants.SERIE_TOP_RATED:
                makeRequest(Constants.TOP_RATED_CATEGORY, Constants.TYPE_SERIE, Constants.FULL_INFORMATION);
                break;
            case Constants.FULL_INFORMATION:
                saveMovies(movieList);
                break;
        }
    }

    private void makeRequest(final String category, final String type, final int typeInformation) {
        Log.e("makeRequest", " ***** Begin *****");
        ApiInterface iClient = ApiClient.getClient().create(ApiInterface.class);
        Call<MovieResponse> call = iClient.getMovies(type,category);
        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                if (response.message().equals("OK")){
                    try {

                        for (Movie m: response.body().getResults()){
                            m.setType(type);
                            m.setCategory(category);
                            movieList.add(m);
                        }

                        getMovies(typeInformation);

                    }catch (Exception e){
                        resultsManager.errorLoad(context.getResources().getString(R.string.msg_error_internet));
                        Log.e("Error Catch Movie",e.getMessage().toString());
                    }
                }else {
                    resultsManager.errorLoad(context.getResources().getString(R.string.msg_error_internet));
                    Log.e("Error Server Movie",response.message().toString());
                }
            }
            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                resultsManager.errorLoad(context.getResources().getString(R.string.msg_error_internet));
                Log.e("MakeRequest.OnFailure",t.getMessage().toString());
                loadFragments();
            }
        });

    }

    @Override
    public void actionSnack() {
        resultsManager.initLoad();
        getMovies(Constants.MOVIE_POPULAR);
    }

    @Override
    public void actionAlert() {
        getListMovies();
    }

    @Override
    public void saveMovies(List<Movie> movieList) {
        resultsManager.loadOk();

        Realm realm = Realm.getDefaultInstance();
        deleteLocalMovies(realm);
        Log.e("HOME","Begin SAVED MOVIES in local");
        for (Movie m: movieList){
            realm.beginTransaction();
            realm.copyToRealm(m);
            realm.commitTransaction();
            Log.i("Saved Movie","Movie: "+m.getName());
        }

        loadFragments();
    }



    private void deleteLocalMovies(Realm realm){
        RealmResults<Movie> localMovies = realm.where(Movie.class).findAll();
        realm.beginTransaction();
        localMovies.deleteAllFromRealm();
        realm.commitTransaction();
        Log.e("HOME","local movies were DELETED");
    }

    public void loadFragments() {
        Log.e("SetFragments", " ***** Begin *****");
        homeInterface.setFragments(homeInterface.createFragmentList());
    }


}
